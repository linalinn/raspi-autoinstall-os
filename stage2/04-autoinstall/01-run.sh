#!/bin/bash -e

install -m 755 files/autoinstall  "${ROOTFS_DIR}/bin/"
install -m 644 files/autoinstall.service "${ROOTFS_DIR}/etc/systemd/system"
on_chroot << EOF
systemctl enable autoinstall
EOF
